Rails.application.routes.draw do

  # get 'daily_sales/index'

  # get 'order_lines/index'
  get 'orders/summary'
  get '/daily_sales' => 'orders#daily_sales', :as => :daily_sales
  get '/cashiers/ranking'
  get '/guests/products' => 'guests#products'
  get '/guests/meat' => 'guests#meat'
  get '/guests/produce' => 'guests#produce'
  get '/guests/cannedgoods' => 'guests#canned_goods'
  get '/guests/sweets' => 'guests#sweets'
  get '/guests/hygiene' => 'guests#hygiene'
  get 'cashiers/daily_sales' => 'cashiers#daily_sales'

  # get 'cashiers/index'

  devise_for :cashiers
  # , :controller => {:registrations => 'cashiers/registrations'}
  devise_for :admins
 
  root 'home#index'
  # devise_scope :admin do
  resources :home, :cashiers, :products, :orders, :order_lines, :daily_sales
  # map.orders '/daily_sales', :controller => 'order', :action => 'dailysales'
  # resources :orders go 
  #   get 'summary', :on => :collection
  # end
  # resources :orders, :path_prefix => ':cashiers'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end

