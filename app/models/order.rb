class Order < ApplicationRecord
  belongs_to :cashier
  has_many :products
  # , through: :order_lines
  has_many :order_lines
  
  accepts_nested_attributes_for :order_lines
  accepts_nested_attributes_for :products
end
