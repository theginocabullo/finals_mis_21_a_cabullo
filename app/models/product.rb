class Product < ApplicationRecord
    belongs_to :admin
    # , optional: true
    has_many :orders, through: :order_lines
    has_many :order_lines
    #belongs_to :order
end
