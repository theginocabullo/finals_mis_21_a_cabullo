class Cashier < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable,
         :recoverable, :rememberable, :trackable, :validatable
         
  # validate :password :on :update, allow_blank => true
  belongs_to :admin, :optional => true
  has_many :orders
  
  def active_for_authentication?
    super && status
  end
end