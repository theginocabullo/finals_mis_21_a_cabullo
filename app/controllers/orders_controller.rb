class OrdersController < ApplicationController
  before_action :set_order, only: [:update, :destroy, :edit, :show]
  def index
    @orders = Order.all
  end

  def new
    @order = Order.new
  end

  def create
    @order = Order.new(order_params)
    @order.cashier = current_cashier
    if @order.save
      # redirect_to orders_summary_path
      render :summary
    else
      render :new
    end
  end
  
  def edit
  end
  
  def destroy
    redirect_to orders_path
  end
  
  def update
    # @order = Order.new(order_params)
    # @order.cashier = current_cashier
    @order = Order.where(:total_price => nil).first
    @order.update(order_params)
    # change = @order.cash - @order.total_price
    # if (change < 0)
    #   render :summary
    # else
      render :show
    # end
    
    # if @order.save
    #   redirect_to orders_path
      # render :show
    # else
    #   render :new
    # end
  end
  
  def show
    @order = Order.find_by(id: params[:id])
    # if @order.cash < @order.total_price
    #   redirect_to orders_summary_path(@order)
    # end 
  end
  
  def summary
    # @orderlines = @order.order_lines
    # @order_lines
    # @order = Order.new(order_params)
    # @order.cashier = current_cashier
    # render :summary
    # @order = Order.where(:total_price => nil).first
    @order = Order.find_by(id: params[:id])
  end 
  
  def daily_sales
    @orders = Order.all
  end
  

  private
    def order_params
      params.require(:order).permit!
    end
    
    def set_order
        @order = Order.find_by(id: params[:id])
        if !@order.present?
          redirect_to orders_path
        end
    end
end
