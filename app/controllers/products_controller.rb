class ProductsController < ApplicationController
  #before_action :authenticate_admin!
  before_action :set_product, only: [:update, :destroy, :edit, :show]
  
  def index
    @products = Product.all
  end
  
  def new
    @product = Product.new
  end
  
  def create
    @product = Product.new(product_params)
    @product.status = true
    @product.admin = current_admin
    if @product.save
      redirect_to products_path
    else
      render :new
    end
  end
  
  def edit

  end
  
  def destroy
    if @product.status
      @product.update({:status => false})
    else
      @product.update({:status => true})
    end
    redirect_to products_path
  end
  
  def update
    @product.update(product_params)
    redirect_to products_path
  end
  
  def show
  
  end
  
  private
    def product_params
      params.require(:product).permit!
    end
    
    def set_product
        @product = Product.find_by(id: params[:id])
        if !@product.present?
          redirect_to products_path
        end
    end
end
