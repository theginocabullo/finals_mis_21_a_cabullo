class GuestsController < ApplicationController
  
  def index
    render :layout => false
  end

  def show
    @products = Product.all
  end
  
  def products 
    @products = Product.all
    render :layout => false
  end
  
  def meat
    @active = Product.where(:status => true)
    @products = @active.where(:tag => "Meat")
    if @products.size == 0
      redirect_to guests_products_path, :alert => "No products available in this category."
    else
      render :meat, :layout => false
    end
  end
  
  def produce
    @active = Product.where(:status => true)
    @products = @active.where(:tag => "Produce")
    if @products.size == 0
      redirect_to guests_products_path, :alert => "No products available in this category."
    else
      render :produce, :layout => false
    end
  end
  
  def canned_goods
    @active = Product.where(:status => true)
    @products = @active.where(:tag => "Canned Goods")
    if @products.size == 0
      redirect_to guests_products_path, :alert => "No products available in this category."
    else
      render :cannedgoods, :layout => false
    end
  end
  
  def sweets
    @active = Product.where(:status => true)
    @products = @active.where(:tag => "Sweets")
    if @products.size == 0
      redirect_to guests_products_path, :alert => "No products available in this category."
    else
      render :sweets, :layout => false
    end
  end
  
  def hygiene
    @active = Product.where(:status => true)
    @products = @active.where(:tag => "Hygiene")
    if @products.size == 0
      redirect_to guests_products_path, :alert => "No products available in this category."
    else
      render :hygiene, :layout => false
    end
    
    # render :layout => false
  end
end
