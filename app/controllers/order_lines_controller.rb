class OrdersController < ApplicationController
  before_action :set_order, only: [:update, :destroy, :edit, :show]
  def index
    @orders = Order.all
  end

  def new
    @orders = Order.all
    @order = Order.new
  end

  def create
    @order = Order.new(order_params)
    @order.cashier = current_cashier
    if @order.save
      # redirect_to orders_summary_path
      render :summary
    else
      render :new
    end
    
    if @order.total_price == nil then @order.destroy end
    end
  end
  
  def edit
  end
  
  def destroy
    redirect_to orders_path
  end
  
  def update
    # @order = Order.new(order_params)
    # @order.cashier = current_cashier
    # if (cash < :total_price)
    if @order.cash < @order.total_price
      # redirect_to orders_summary_path
      render :summary
    end
    @order = Order.where(:total_price => nil).first
    @order.update(order_params)
    
    # @orders = Order.all
    # render :show
    # if @order.save
    #   redirect_to orders_path
      # render :show
    # else
    #   render :new
    # end
  end
  
  def show
    @order = Order.find_by(id: params[:id])
    # if @order.cash < @order.total_price
    #   redirect_to orders_summary_path(@order)
    # end
  end
  
  def summary
    # @orderlines = @order.order_lines
    # @order_lines
    # @order = Order.new(order_params)
    # @order.cashier = current_cashier
    # render :summary
    # @order = Order.where(:total_price => nil).first
    @order = Order.find_by(id: params[:id])
    @orders = Order.all
  end 
  
  def daily_sales
    @orders = Order.all
  end

  private
    def order_params
      params.require(:order).permit!
    end
    
    def set_order
        @order = Order.find_by(id: params[:id])
        if !@order.present?
          redirect_to orders_path
        end
    end
end
