class CashiersController < ApplicationController
  # before_action :authenticate_admin!
  before_action :set_cashier, only: [:update, :destroy, :edit, :show]
  
  def index
    @cashiers = Cashier.all
  end
  
  def new
    @cashier = Cashier.new
  end
  
  def create
    @cashier = Cashier.new(cashier_params)
    @cashier.admin = current_admin
    if @cashier.save
      redirect_to cashiers_path
    else
      render :new
    end
  end
  
   def destroy
    if @cashier.status
      @cashier.update({:status => false})
      # @cashier.status == false
    else
      @cashier.update({:status => true})
      # @cashier.status == true
    end
    redirect_to cashiers_path
   end
  
  def edit
  end
  
 def update
    if params[:cashier][:password].blank? && params[:cashier][:password_confirmation].blank?
      params[:cashier].delete(:password)
      params[:cashier].delete(:password_confirmation)
    end
    @cashier.update(cashier_params)
    redirect_to cashiers_path
  end
  
  def show
    @orders = @cashier.orders
  end
  
  def ranking
    @cashiers = Cashier.all
    @orders = Order.all
  end
  
  def daily_sales
    @orders = Order.all
  end
  
  private
    def cashier_params
      params.require(:cashier).permit!
    end
    
    def set_cashier
        @cashier = Cashier.find_by(id: params[:id])
        if !@cashier.present?
          redirect_to cashiers_path
        end
    end
    
    protected
        def sign_up(resource_name, resource)
            # sign_in(resource_name, resource)
        end
        
        def update_resource(resource, params)
            resource.update_without_password(params)
        end
        
        def update_needs_confirmation?(resource, previous)
        #resource.respond_to?(:pending_reconfirmation?) &&
        #resource.pending_reconfirmation? &&
        #previous != resource.unconfirmed_email
        end
end
