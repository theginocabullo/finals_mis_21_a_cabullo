class AddNewTotalPriceToOrders < ActiveRecord::Migration[5.1]
  def change
    add_column :orders, :new_total_price, :decimal
  end
end
