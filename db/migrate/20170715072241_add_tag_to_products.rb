class AddTagToProducts < ActiveRecord::Migration[5.1]
  def change
    add_column :products, :tag, :string
  end
end
