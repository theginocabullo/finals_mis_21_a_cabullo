class AddVatToOrders < ActiveRecord::Migration[5.1]
  def change
    add_column :orders, :vat, :decimal
  end
end
