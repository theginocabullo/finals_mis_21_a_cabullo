class CreateOrders < ActiveRecord::Migration[5.1]
  def change
    create_table :orders do |t|
      t.decimal :total_price
      t.decimal :cash
      t.references :cashier, foreign_key: true

      t.timestamps
    end
  end
end
